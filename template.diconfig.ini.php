;<?php die; ?>
;This stops the server from displaying the file

;------------------------------------------------------------------------------
;This is the default section, more sections can be added if needed
;------------------------------------------------------------------------------
[default]
username="100"
password="password"

;the url of our api
post_url=""

;list information
list_id="0000000"
external_key="0000000"
agent="1001"

;the fake field is supposed to be submitted as blank
;fake="gender"

;debug info:
;if you need to debug, put your email address here and you'll
;be copied on a response from the API
debug="me@site.com"
